# PandoraPi_Website

This repository contains all of the files and scripts that are necessary to add web based control to the Adafruit PiPhi project.  Once you are finished completing that tutorial to get your Raspberry Pi Pandora Radio streamer up and running.  You can then install Apache2 to the Raspberry Pi device and control its functions from a convenient web based interface.  This is ideal for people who do not know command line actions very well and need a gui to connect to in order to update and perform other functions on the Raspberry Pi device.

Here: https://learn.adafruit.com/pi-wifi-radio/ is where you can find the PiPhi Radio

You will also need to install Apache2 as your webserver and configure it to run python scripts with the cgi-engine.

Created a new program that can be found and compiled from the masterblaster folder within the repo.  This creates a program that has root access and will accomplish many of the things a standard user cannot such as updating, restarting, and shutting down the system.  Instructions on how to edit and compile this file are located within its readme in the masterblaster directory.

I plan to add more features to the web interface such as a way to see the current playing track and other console type information from the python scripts as they are run in the future.
