#!/usr/bin/python

import subprocess
import time
import thread

#Update the pianobar git repo
def update():
	print ("Content-type: text/html")
	print ()
	print ("Pulling the repo")
	command = "cd /home/pi/pianobar && git pull"
	print ("Cleaning the old build out")
	command = "make clean"
	print ("Building the new one and copying to the right location")
	command = "make"
	command = "/usr/local/bin/masterblaster -P"
	process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
	output = process.communicate()[0]
	process.wait()
	
#restart the operating system
def restart():
	print ("Content-type: text/html")
	print ()
	print ("Restarting the machine")
	command = "/usr/bin/sudo /sbin/reboot"
	process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
	output = process.communicate()[0]
	process.wait()
	
#Redirect back to the site when done
def redirect():
	print ("Content-type: text/html")
	print ()
	print ("""
		<html>
			<head>
				<title>Redirect Page</title>
				<meta http-equiv="refresh" content="0; url=http://localhost/" />
			</head>
		</html>
	""")
	
#create a time delay function
def spinner():
	while True:
		print ('.')
		time.sleep(1)

#create main method
if __name__ == '__main__':
	thread.start_new_thread(spinner, ())
	update()
	time.sleep(2)
	thread.start_new_thread(spinner, ())
	restart()
	time.sleep(2)
