#!/usr/bin/python

#import Libraries
import cgi
import cgitb

# Create instance of FieldStorage
form = cgi.FieldStorage()

# Get data from fields
emailf = form.getvalue('email')
passwordf  = form.getvalue('password')
config_file = '/home/pi/.config/pianobar/config'
userv = 'user'
passwordv = 'password'

#Kill the pianobar service
def kpianobar():
	print ("Content-type: text/html")
	print ()
	print ("Killing the pandora service")
	command = "/usr/local/bin/masterblaster -k pianobar"
	process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
	output = process.communicate()[0]
	process.wait()
	
#Restart pianobar after updates
def spianobar():
	print ("Content-type: text/html")
	print ()
	print ("Starting the pandora service")
	command = "/usr/local/bin/masterblaster -p"
	process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
	output = process.communicate()[0]
	process.wait()

#Use modconfig to update the user information
def updateuser():
	command = "/usr/local/bin/masterblaster -m %s %s %s" % (config_file, userv, emailf)
	process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
	output = process.communicate()[0]
	process.wait()

#Use modconfig to update the password information
def updatepassword():
	command = "/usr/local/bin/masterblaster -m %s %s %s" % (config_file, passwordv, passwordf)
	process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
	output = process.communicate()[0]
	process.wait()

#Redirect back to the site when done
def redirect():
	print ("Content-type: text/html")
	print ()
	print ("""
		<html>
			<head>
				<title>Redirect Page</title>
				<meta http-equiv="refresh" content="0; url=http://localhost/" />
			</head>
		</html>
	""")
	
#create a time delay function
def spinner():
	while True:
		print ('.')
		time.sleep(1)

#create main method
if __name__ == '__main__':
	thread.start_new_thread(spinner, ())
	kpianobar()
	thread.start_new_thread(spinner, ())
	updateuser()
	time.sleep(2)
	thread.start_new_thread(spinner, ())
	updatepassword()
	thread.start_new_thread(spinner, ())
	spianobar()
	time.sleep(2)
	redirect()
	
#HTML Example
#<form action="/cgi-bin/hello_get.py" method="get">
#Email: <input type="text" name="email">  <br />
#Password: <input type="text" name="password" />
#<input type="submit" value="Submit" />
#</form>
