#!/usr/bin/python

#import Libraries
import cgi
import cgitb
import time
import thread
import subprocess

# Create instance of FieldStorage
form = cgi.FieldStorage()

# Get data from fields
wifif = form.getvalue('wifi')
wifif = '"%s"'%wifif
passwordf  = form.getvalue('password')
passwordf = '"%s"'%passwordf
sudo = '/usr/bin/sudo'
modconfig = '/var/www/html/modconfig.py'
config_file = '/etc/wpa_supplicant/wpa_supplicant.conf'
userv = 'ssid'
passwordv = 'psk'

#Use modconfig to update the user information
def updateuser():
	print ("Updating the SSID")
	command = "%s %s %s %s %s" % (sudo, modconfig, config_file, userv, wifif)
	print (command)
	process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
	output = process.communicate()[0]
	process.wait()

#Use modconfig to update the password information
def updatepassword():
	print ("Updating the PSK")
	command = "%s %s %s %s %s" % (sudo, modconfig, config_file, passwordv, passwordf)
	print (command)
	process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
	output = process.communicate()[0]
	process.wait()

#Restart to take effect
def restart():
	command = "/usr/bin/sudo /sbin/reboot"
	process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
	output = process.communicate()[0]
	
#create a time delay function
def spinner():
	while True:
		print ('.')
		time.sleep(1)

#create main method
if __name__ == '__main__':
	thread.start_new_thread(spinner, ())
	updateuser()
	time.sleep(2)
	thread.start_new_thread(spinner, ())
	updatepassword()
	time.sleep(2)
	restart()
	
#HTML Example
#<form action="/cgi-bin/hello_get.py" method="get">
#Email: <input type="text" name="email">  <br />
#Password: <input type="text" name="password" />
#<input type="submit" value="Submit" />
#</form>
