The purpose of this program is to enable sudo commands without actually opening any security holes in the operating system.  The file needs to be compiled and can be modifyed by the user to add or remove some of the functions that it performs.  Currently I have it performing all of the actions the website calls for through its buttons that require any type of sudo elevation.  Again this can be changed to suit your needs so do that before compiling.

INSTALLATION
1. cd into masterblaster folder within the main website directory
2. Edit main.cpp to include or remove whatever you do not need in the program
3. type make
4. type sudo make install
5. At this point the program should be built and installed in the /usr/local/bin directory with the correct suid to run the commands needed as root when called on from the www-data user.
