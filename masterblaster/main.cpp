#include <iostream>
#include <cstdlib>

using namespace std;

const char * REBOOT_CMD = "/sbin/reboot";
const char * SHUTDOWN_CMD = "/sbin/poweroff";
const char * KILLP_CMD = "/usr/bin/pkill pianobar";
const char * PANDORA_CMD = "/usr/bin/python /home/pi/Python-WiFi-Radio/PiPhi.py";
const char * UPDATE_CMD1 = "/usr/bin/apt-get update";
const char * UPDATE_CMD2 = "/usr/bin/apt-get upgrade -y";
const char * UPDATEP_CMD = "cp /home/pi/pianobar/pianobar /usr/local/bin";
const char * MODIFYP_CMD = "/var/www/html/modconfig.py";

int main(int argc, char* argv[]) {
  if (argc < 2) {
    cout << endl << "  Usage: " << argv[0] << " { -r | -s | -k | -p | -u | -U | -P | -m }"<< endl << endl;
    cout << "	-r	Reboot system" << endl;
    cout << "	-s	Shutdown system" << endl;
    cout << "	-k	Kill Pandora" << endl;
    cout << "	-p	Start pandora" << endl;
    cout << "	-u	Update system" << endl;
    cout << "	-U	Upgrade system" << endl;
    cout << "	-P	Update Pianobar" << endl;
    cout << "	-m	Modify Pianobar" << endl;
    cout << endl;
    return -1;
  }

  string checkme = argv[1];

  if (checkme.compare("-r") == 0) {
    system(REBOOT_CMD);
  } else if (checkme.compare("-s") == 0) {
    system(SHUTDOWN_CMD);
  } else if (checkme.compare("-k") == 0) {
    system(KILLP_CMD); 
  } else if (checkme.compare("-p") == 0) {
    system(PANDORA_CMD);
  } else if (checkme.compare("-u") == 0) {
    system(UPDATE_CMD1);
  } else if (checkme.compare("-U") == 0) {
    system(UPDATE_CMD2);
  } else if (checkme.compare("-P") == 0) {
    system(UPDATEP_CMD);
  } else if (checkme.compare("-m") == 0) {
    system(MODIFYP_CMD);
  }
  return 0;
}
