#!/usr/bin/python

#import Libraries
import subprocess
import time
import thread

#Update the headers and such
def update():
	print ("Content-type: text/html")
	print ()
	print ("Updating the headers")
	command = "/usr/local/bin/masterblaster -u"
	process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
	output = process.communicate()[0]
	process.wait()
	
#Upgrade the operating system
def upgrade():
	print ("Content-type: text/html")
	print ()
	print ("Upgrading the operating system")
	command = "/usr/local/bin/masterblaster -U -y"
	process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
	output = process.communicate()[0]
	process.wait()
	
#Redirect back to the site when done
def redirect():
	print ("Content-type: text/html")
	print ()
	print ("""
		<html>
			<head>
				<title>Redirect Page</title>
				<meta http-equiv="refresh" content="0; url=http://localhost/" />
			</head>
		</html>
	""")
	
#create a time delay function
def spinner():
	while True:
		print ('.')
		time.sleep(1)

#create main method
if __name__ == '__main__':
	thread.start_new_thread(spinner, ())
	update()
	time.sleep(2)
	thread.start_new_thread(spinner, ())
	upgrade()
	time.sleep(2)
	redirect()
