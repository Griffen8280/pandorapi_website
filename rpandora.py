#!/usr/bin/python

#import Libraries
import subprocess
import time
import thread

#Kill the service first
def kpianobar():
	print ("Content-type: text/html")
	print ()
	print ("Killing the pandora service")
	command = "/usr/local/bin/masterblaster -k pianobar"
	process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
	output = process.communicate()[0]
	process.wait()
	
#Setup the restart function
def spianobar():
	print ("Content-type: text/html")
	print ()
	print ("Starting the pandora service")
	subprocess.Popen(["nohup", "/usr/local/bin/masterblaster -p"])
	#process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
	#output = process.communicate()[0]
	
#Redirect back to the site when done
def redirect():
	print ("Content-type: text/html")
	print ()
	print ("""
		<html>
			<head>
				<title>Redirect Page</title>
				<meta http-equiv="refresh" content="0; url=http://localhost/" />
			</head>
		</html>
	""")
	
#create a time delay function
def spinner():
	while True:
		print ('.')
		time.sleep(1)

#create main method
if __name__ == '__main__':
	thread.start_new_thread(spinner, ())
	kpianobar()
	time.sleep(2)
	thread.start_new_thread(spinner, ())
	spianobar()
	time.sleep(2)
	redirect()
